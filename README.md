# 💀 Dead Man's Switch

⚠️ WORK IN PROGRESS ! ⚠️

## Why
If somethings happen to you the people you choose will have access to a file you prepared.  
The content of the file will be a secret to everyone until the triggering of the switch.  
This concept is used by journalists, activists and hackers as "insurance" from repercussions for the work they do.  

## How
Using Shamir's Secret Sharing and Blockchain it is possible to share information to multiple parties without them knowing the content.  
In order to know the content most of the parties must agree that you are incapacitated.  
You have complete freedom in deciding the parties involved, the percentage of parties needed to decrypt the information and who should know the information.  

## Security
Shamir's Secret Sharing is proven to be secure, minimal, extensible, dynamic and flexible ([Shamir's Secret Sharing properties](https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing#Properties)).  
The blockchain technology has been tested for over 10 years successfully ([Blockchain vulnerabilities](https://resources.infosecinstitute.com/blockchain-vulnerabilities-imperfections-of-the-perfect-system/)).  
All communications are encrypted ([BIP 151](https://github.com/bitcoin/bips/blob/master/bip-0151.mediawiki)).  

# History
This is not a new concept, below some examples:
* [**Dead Hand**](https://en.wikipedia.org/wiki/Dead_Hand): Cold War-era automatic nuclear weapons-control system used by the Soviet Union to [second-strike](https://en.wikipedia.org/wiki/Second_strike) when a nuclear attack is detected, even if the enemy blew up the Kremlin, took out the defense ministry, severed the communications network, and killed commanding officers.
* [**Snowden's Dead Man's Switch**](https://www.wired.com/2013/07/snowden-dead-mans-switch/): Snowden distributed sets of the information in such a fashion that if he is taken, then other people will move to release information. His positive control of the system is not required in fact, it’s his negative control that applies.
* [**WikiLeaks' insurance**](https://twitter.com/wikileaks/status/743824112376766465): "Protect our coming publications. Torrent WIKILEAKS INSURANCE 2016-06-03 (88 Gb encrypted)"
* [**Cthulhu's insurance**](https://web.archive.org/web/20170216133846/https://www.thecthulhu.com/insurance-release/): "[...] many thanks for everyone sharing the file and taking a copy for reference in future should it be released. I do not believe in giving control of material to a single group of journalists, for it does not allow for proper interrogation of the data, nor do I believe any one group of people should be entitled to privileged access in a matter of public interest. For that reason, I made it distributed in a way I believe to be fairest to all."

The issue with having the encrypted file publicly distributed and giving the decryption key a group of people is that that group will have access to the information anytime, which requires them to be completely trusted.
These people can be identified and attacked, physically or informatically, putting the information and the people at risk.







# Use cases

### Concept
* File to share (unencrypted and preferrably compressed) (raw max. 1 MB\magnet torrent\external link)
* Untrusted party (all other users)
* Semi-trusted party (system)
* Trusted parties (one or more specific user)
* Minimum parties to decrypt (minimum:2)
* Parties that will actually decrypt (default: all) (it has to be compatible with the precedent parameter)

### Examples:

#### Journalist that is investigating on evil corporation, if something happen to him he want the truth to be revealed

| **Parameter** | **Value** |
| :------ | :-----: |
| File to share | evilcorp.zip |
| Untrusted party | Yes |
| Semi-trusted party | Yes |
| Trusted parties | husband, brother, colleague |
| Minimum parties to decrypt | 2 |
| Parties that will actually decrypt | All |

Parts of key assigned to each party:
* Untrusted party: 1
* Semi-trusted party: 1
* husband: 1
* brother: 1
* colleague: 1

Parts needed to reconstruct the secret: 2

---

#### Father that wants to leave to his childrens his will

| **Parameter** | **Value** |
| :------ | :-----: |
| File to share | will.docx |
| Untrusted party | No |
| Semi-trusted party | No |
| Trusted parties | wife, son, daughter, mother-in-law, friend |
| Minimum parties to decrypt | 3 |
| Parties that will actually decrypt | son, daughter |

Parts of key assigned to each party:
* wife: 1
* son: 4
* daughter: 4
* mother-in-law: 1
* friend: 1

Parts needed to reconstruct the secret: 5

---

#### Political activist that trust noone and want to share his finding if arrested

| **Parameter** | **Value** |
| :------ | :-----: |
| File to share | magnet:?xt=urn:btih:3d1052d5... |
| Untrusted party | Yes |
| Semi-trusted party | Yes |
| Trusted parties | None |
| Minimum parties to decrypt | 2 |
| Parties that will actually decrypt | All |

Parts of key assigned to each party:
* Untrusted party: 1
* Semi-trusted party: 1

Parts needed to reconstruct the secret: 2

---

## Sequence Diagram
```mermaid
sequenceDiagram
    participant A as Alice
    participant P as Parties
    participant B as Public Blockchain
    
    A->>A: Encrypt file using x keys
    A->>P: Send x keys each one to each party
    loop Not all acknowledgments received
        P->>B: Send acknowledge for receiving a key (hash to verify correctness at the end)
        A-->>B: Check all acknowledgments
    end
    A->>B: Send encrypted file
    
    loop Last control message valid
        A->>B: Send control message
        P-->>B: Verify last block validity (use deepness)
        P-->>B: Check control message
    end
    P->>B: Send decryption keys (if planned)
```

# Technology

* [Shamir's Secret Sharing demo](https://iancoleman.io/shamir/)
* ...

# Other solutions
* Service to send mail if operator doen't trigger the event (ex. deadmansswitch.net)
* Service to send delayed mail (ex. lettermelater.com, emailfuture.com)
* Mail client which send delayed mail (ex. Gmail+Streak, Outlook, Thunderbird+Send Later)

# Usage example:
* Alice wants a File to be published if she doesn’t trigger an Event after the ExpirationTime (ex. she is arrested).
* Alice encrypt the File using a new random Key, creating an EncryptedFile.
* Alice joins DMS and generates a PUblicKey and a PRivateKey only for DMS, sharing her PUblicKey with other DMS users.
* Alice decides who are the parties involved and how many of them are needed to decrypt the EncryptedFile. This decision is a balance between availability and confidentiality.
  *	Untrusted-Party (unknown users splitted in groups)
  *	Semi-Trusted-Party (provided by the system)
  * Trusted-Party (ex. Bob)
* Alice decides the ExpirationTime for the non-event (ex. 1 month) and the MaximumDuration.
* Alice split the Key used to encrypt the File into SplittedKeys (Shamir's Secret Sharing) and send it to the InvolvedParties.
* Once all the InvolvedParties receive the SplittedKeys and send an Acknowledgment to Alice, Alice publish the EncryptedFile.
* When the ExpirationTime has passed (if the MaximumDuration han’t passed) all the InvolvedParties verify autonomously if the Event was triggered (ex. send a timestamp signed with her PRivateKey).
* If enough parties agree that the Event was not triggered, they will join their SplittedKeys, decrypting the EncryptedFile.

# Visibility example
```mermaid
sequenceDiagram
    participant A as Alice
    participant B as Bob
    participant C as Charlie
    participant D as David
    participant E as Erin
    participant BL as Blockchain
    
    BL->>BL: Owner is "dead"
    C->>BL: My secret is "charliessecret"
    D->>BL: My secret is "davidssecret"
    Note over E: Erin is offline
    BL-->>A: Read Charlie/David part of the secret
    A->>A: Decrypt secret
    BL-->>B: Read Charlie/David part of the secret
    B->>B: Decrypt secret
 ```

